export class ToDoItem {
  constructor(
    public id?: string | number,
    public label?: string,
    public description?: string,
    public category?: string,
    public done?: boolean | string,
  ) { }
}