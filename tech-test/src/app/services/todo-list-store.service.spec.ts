import { TestBed } from '@angular/core/testing';

import { TodoListStoreService } from './todo-list-store.service';

describe('TodoListStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TodoListStoreService = TestBed.get(TodoListStoreService);
    expect(service).toBeTruthy();
  });
});
