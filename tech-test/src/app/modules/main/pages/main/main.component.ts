import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { isNullOrUndefined } from 'util';
import { TodoListStoreService } from '../../../../services/todo-list-store.service';
import { ToDoItem } from '../../../../shared/models/todo-item.model';
import { CreateTodoDialogComponent } from '../../components/create-todo-dialog/create-todo-dialog.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  selectedCategory: string;
  showOpen: boolean = false;
  filteredToDoList = [];
  toDoList = [];

  constructor(
    private todoListStoreService: TodoListStoreService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.todoListStoreService.fetchTodoItems()
      .subscribe(res => {
        this.toDoList = res;
        this.filteredToDoList = res;
      })
  }

  updateData(item: ToDoItem, index: number) {
    this.todoListStoreService.updateTodoItem(item).subscribe(
      res => {
        this.toDoList[index] = item;
        this.toDoList = [...this.toDoList];
        this.filteredToDoList = this.toDoList;
      }
    )
  }

  deleteData(id: string | number) {
    this.todoListStoreService.deleteTodoItem(id).subscribe();
    this.toDoList = this.toDoList.filter(it => it.id !== id);
    this.filteredToDoList = this.toDoList;
  }

  filterData() {
    this.filteredToDoList = this.toDoList
      .filter(it => (isNullOrUndefined(this.selectedCategory) || (this.selectedCategory && it.category === this.selectedCategory)) &&
        ((this.showOpen && it.done === false) || !this.showOpen));
  }

  applyCategoryFilter(category?: string) {
    this.selectedCategory = category;
    this.filterData();
  }

  applyOpenFilter(showOpen: boolean) {
    console.log(showOpen)
    this.showOpen = showOpen;
    this.filterData();
  }

  trackToDoListById(index, item) {
    return item.id;
  }

  clickAddToDoHandler() {
    const dialogRef = this.dialog.open(CreateTodoDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe((res: ToDoItem) => {
      if (res) {
        this.toDoList = [res, ...this.toDoList];
        this.filteredToDoList = this.toDoList;
      }
    });
  }

  toggleColorBackgroundHandler() {
    const body = document.getElementsByTagName('body')[0];
    if (body.classList.contains('color__bg')) {
      body.classList.remove('color__bg');   //remove the class
    } else {
      console.log('hi')
      body.classList.add('color__bg');   //add the class
    }
  }
}
