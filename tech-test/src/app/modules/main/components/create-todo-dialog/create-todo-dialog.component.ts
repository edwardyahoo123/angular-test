import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TodoListStoreService } from '../../../../services/todo-list-store.service';
import { ToDoItem } from '../../../../shared/models/todo-item.model';

@Component({
  selector: 'app-create-todo-dialog',
  templateUrl: './create-todo-dialog.component.html',
  styleUrls: ['./create-todo-dialog.component.scss']
})
export class CreateTodoDialogComponent implements OnInit {
  newToDo = new ToDoItem();

  constructor(
    public dialogRef: MatDialogRef<CreateTodoDialogComponent>,
    private todoListStoreService: TodoListStoreService,
  ) { }

  ngOnInit() {
    this.newToDo.category = '';
    this.newToDo.label = '';
    this.newToDo.description = '';
    this.newToDo.done = false;
  }

  clickCancelHandler(data?: ToDoItem): void {
    this.dialogRef.close(data);
  }

  clickSaveHandler(): void {
    this.todoListStoreService.createTodoItem(this.newToDo).subscribe(res => {
      this.clickCancelHandler(res);
    });
  }
}
