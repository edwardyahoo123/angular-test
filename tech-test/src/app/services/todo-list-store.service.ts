import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ToDoItem } from '../shared/models/todo-item.model';

@Injectable({
  providedIn: 'root'
})
export class TodoListStoreService {
  readonly apiPath = environment.apiUrl + '/tasks';

  constructor(
    private http: HttpClient
  ) { }

  fetchTodoItems(): Observable<ToDoItem[]> {
    return this.http.get<ToDoItem[]>(this.apiPath);
  }

  updateTodoItem(item: ToDoItem): Observable<ToDoItem> {
    return this.http.patch<ToDoItem>(this.apiPath + '/' + item.id, item);
  }

  createTodoItem(item: ToDoItem): Observable<ToDoItem> {
    return this.http.post<ToDoItem>(this.apiPath, item);
  }

  deleteTodoItem(id: string | number): Observable<any> {
    return this.http.delete(this.apiPath + '/' + id);
  }
}
