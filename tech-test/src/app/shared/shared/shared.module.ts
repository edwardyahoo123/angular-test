import { NgModule } from '@angular/core';
import { 
  MatCardModule, 
  MatIconModule,
  MatSlideToggleModule, 
  MatInputModule,
  MatButtonModule,
  MatChipsModule,
  MatDialogModule,
} from '@angular/material';



@NgModule({
  exports: [
    MatCardModule,
    MatIconModule,
    MatSlideToggleModule,
    MatInputModule,
    MatButtonModule,
    MatChipsModule,
    MatDialogModule,
  ]
})
export class SharedModule { }
