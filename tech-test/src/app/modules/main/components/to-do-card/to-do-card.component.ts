import { animate, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToDoItem } from '../../../../shared/models/todo-item.model';

@Component({
  selector: 'app-to-do-card',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({opacity: 0}),
          animate('200ms', style({opacity: 1}))
        ]),
        transition(':leave', [
          style({opacity: 1}),
          animate('200ms', style({opacity: 0}))
        ])
      ]
    )
  ],
  templateUrl: './to-do-card.component.html',
  styleUrls: ['./to-do-card.component.scss']
})
export class ToDoCardComponent implements OnInit {
  @Input() item: ToDoItem;
  @Output('onSave') onSaveEmitter = new EventEmitter<ToDoItem>();
  @Output('onDelete') onDeleteEmitter = new EventEmitter<boolean>();

  constructor(
  ) { }

  ngOnInit() {
  }

  parseCheckedToDoneField(checked: boolean): boolean | string {
    if (checked) {
      const today = new Date();
      return `${today.getDate()}-${today.getMonth() + 1}-${today.getFullYear()}`;
    }
    return false;
  }

  updateData(value: any, field: string) {
    this.onSaveEmitter.emit({
      ...this.item,
      [field]: value,
    });
  }

  toggleDoneHandler(event): void {
    const done = this.parseCheckedToDoneField(event.checked);
    this.updateData(done, 'done');
  }

  clickSaveButtonHandler(value, field): void {
    this.updateData(value, field)
  }

  clickDeleteButtonHandler(): void {
    this.onDeleteEmitter.emit(true);
  }
}
