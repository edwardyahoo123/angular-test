import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ToDoItem } from '../../../../shared/models/todo-item.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnChanges {
  @Input() showOpen: boolean;
  @Input() selectedCategory: string;
  @Input() items: ToDoItem[];
  @Output('clickFilter') clickItemEmitter = new EventEmitter<boolean>();
  totalToDo: number = 0;
  totalOpen: number = 0;

  constructor() { }

  ngOnChanges(changes) {
    if (changes.selectedCategory) {
      this.refreshValue();
    }

    if (changes.items) {
      this.refreshValue();
    }
  }

  refreshValue() {
    const fitleredByCat = this.selectedCategory ? this.items.filter(({ category }) => category === this.selectedCategory) : this.items;
    this.totalToDo = fitleredByCat.length;
    this.totalOpen = fitleredByCat.filter(({ done }) => !done).length;
  }

  clickDashboardItemHandler(showOpen: boolean) {
    this.clickItemEmitter.emit(showOpen);
  }

}
