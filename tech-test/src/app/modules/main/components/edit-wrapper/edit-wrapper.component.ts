import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-edit-wrapper',
  templateUrl: './edit-wrapper.component.html',
  styleUrls: ['./edit-wrapper.component.scss']
})
export class EditWrapperComponent implements OnInit, OnChanges {
  @Input() value: string;
  @Input() variant: 'input' | 'textarea';
  @Input() label: string;
  @Input() valueClassName: string;
  @Output() clickSave = new EventEmitter<string>();
  updatedValue: string;
  isEditing: boolean = false;

  constructor() { }

  ngOnChanges(changes) {
    if (changes.value) {
      this.updatedValue = changes.value.currentValue;
    }
  }

  ngOnInit() {
  }

  inputChangeHandler(event) {
    this.updatedValue = event.target.value;
  }

  clickSaveHandler() {
    this.clickSave.emit(this.updatedValue);
    this.toggleEdit();
  }

  toggleEdit() {
    this.isEditing = !this.isEditing;
  }

  clickCancelHandler() {
    this.toggleEdit();
    this.updatedValue = this.value;
  }
}
