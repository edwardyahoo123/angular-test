import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToDoItem } from '../../../../shared/models/todo-item.model';

@Component({
  selector: 'app-item-filter',
  templateUrl: './item-filter.component.html',
  styleUrls: ['./item-filter.component.scss']
})
export class ItemFilterComponent implements OnInit {
  categories: string[];
  @Input() selectedCategory: string;
  @Input('items')
  set items(items: ToDoItem[]) {
    if (items) {
      this.categories = Array.from((new Set(items.map(({ category }) => category))));
    }
  }
  @Output('clickFilter') clickFilterEmitter = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  clickFilter(category: string) {
    if (this.selectedCategory !== category) {
      // this.selectedCategory = category;
      this.clickFilterEmitter.emit(category);
    } else {
      // this.selectedCategory = null;
      this.clickFilterEmitter.emit(null);
    }
  }
}
