import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './pages/main/main.component';
import { ToDoCardComponent } from './components/to-do-card/to-do-card.component';
import { SharedModule } from '../../shared/shared/shared.module';
import { EditWrapperComponent } from './components/edit-wrapper/edit-wrapper.component';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CreateTodoDialogComponent } from './components/create-todo-dialog/create-todo-dialog.component';
import { ItemFilterComponent } from './components/item-filter/item-filter.component';



@NgModule({
  declarations: [MainComponent, ToDoCardComponent, EditWrapperComponent, DashboardComponent, CreateTodoDialogComponent, ItemFilterComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
  ],
  exports: [
    MainComponent
  ],
  entryComponents: [
    CreateTodoDialogComponent
  ]
})
export class MainModule { }
